import React, { useState } from 'react';
import logo from './logo.svg';
import './App.css';

function App() {
  const [name, setName] = useState('there!')

  const nameHandler = (event) => {
    setName(event.target.value)
  }
  return (
    <div className="App">
      <input type='text' onChange={nameHandler} />
      Hello {name}
    </div>
  );
}
//last try for the night kmft

export default App;
